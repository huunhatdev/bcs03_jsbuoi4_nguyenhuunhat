// Bài tập 1
function ex1() {
  var num1 = document.getElementById("num1-1").value * 1;
  var num2 = document.getElementById("num1-2").value * 1;
  var num3 = document.getElementById("num1-3").value * 1;
  var sapXep = null;

  if (num1 < num2 && num2 < num3) {
    sapXep = `${num1}, ${num2}, ${num3}`;
  } else if (num1 < num3 && num3 < num2) {
    sapXep = `${num1}, ${num3}, ${num2}`;
  } else if (num2 < num1 && num1 < num3) {
    sapXep = `${num2}, ${num1}, ${num3}`;
  } else if (num2 < num3 && num3 < num1) {
    sapXep = `${num2}, ${num3}, ${num1}`;
  } else if (num3 < num1 && num1 < num2) {
    sapXep = `${num3}, ${num1}, ${num2}`;
  } else {
    sapXep = `${num3}, ${num2}, ${num1}`;
  }

  document.getElementById(
    "result1"
  ).innerHTML = `Sắp xếp từ nhỏ đến lớn: ${sapXep}`;
}

// Bài tập 2
function ex2() {
  var memVal = document.getElementById("mem").value;
  var mem = null;

  switch (memVal) {
    case "B":
      mem = "Bố";
      break;
    case "M":
      mem = "Mẹ";
      break;
    case "A":
      mem = "Anh Trai";
      break;
    case "E":
      mem = "Em Gái";
      break;
    default:
      mem = "";
  }

  document.getElementById("result2").innerHTML = `Xin chào ${mem}`;
}

// bài tập 3
function ex3() {
  var num1 = document.getElementById("num2-1").value * 1;
  var num2 = document.getElementById("num2-2").value * 1;
  var num3 = document.getElementById("num2-3").value * 1;
  var soChan = 0;
  var soLe = 0;

  if (num1 % 2 === 0) {
    soChan += 1;
  } else {
    soLe += 1;
  }
  if (num2 % 2 === 0) {
    soChan += 1;
  } else {
    soLe += 1;
  }
  if (num3 % 2 === 0) {
    soChan += 1;
  } else {
    soLe += 1;
  }

  document.getElementById(
    "result3"
  ).innerHTML = `Dãy số gồm: ${soChan} số chẵn và ${soLe} số lẻ`;
}

//bài tập 4
function ex4() {
  var num1 = document.getElementById("edge1").value * 1;
  var num2 = document.getElementById("edge2").value * 1;
  var num3 = document.getElementById("edge3").value * 1;

  var pytago1 =
    Math.pow(num1, 2) + Math.pow(num2, 2) === Math.pow(num3, 2) ? true : false;
  var pytago2 =
    Math.pow(num1, 2) + Math.pow(num3, 2) === Math.pow(num2, 2) ? true : false;
  var pytago3 =
    Math.pow(num2, 2) + Math.pow(num3, 2) === Math.pow(num1, 2) ? true : false;

  var triangle = "thường";

  if ((num1 === num2) & (num1 === num3)) {
    triangle = "đều";
  } else if (num1 === num2 || num1 === num3 || num2 === num3) {
    triangle = "cân";
  } else if (pytago1 || pytago2 || pytago3) {
    triangle = "vuông";
  }

  document.getElementById("result4").innerHTML = "Đây là tam giác " + triangle;
}
